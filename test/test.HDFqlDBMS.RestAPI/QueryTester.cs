using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using System.Net.Http;

namespace test._HDFqlDBMS.RestAPI
{
    public class QueryTester
    {
        private readonly TestServer _serverApi;
        private readonly TestServer _serverIdentity;
        private readonly HttpClient _client;

        public QueryTester()
        {
            //var folderExist = System.IO.Directory.Exists(ConfigApi.GetContentRoot());
            
            //if (!folderExist)
            //{
            //    System.IO.Directory.CreateDirectory(ConfigApi.GetContentRoot());
            //}
            var puffer = new string[]{};
            _serverIdentity = new TestServer(IdentityServer.Program.BuildWebHost(puffer)); 

            _serverApi = new TestServer(HDFqlDBMS.RestAPI.Program.BuildWebHost(puffer));

            _client = new HttpClient();
        }
        ~QueryTester()
        {

        }
        [Fact]
        public void TestCreation()
        {
            var disco = IdentityModel.Client.DiscoveryClient.GetAsync("http://localhost:5000");

             if (disco.Result.IsError)
            {
                Console.WriteLine(disco.Result.Error);
                return;
            }

            var response =  _client.GetAsync("http://localhost:5001/api/Query");

            var t = response.Result;
        }
    }
}
