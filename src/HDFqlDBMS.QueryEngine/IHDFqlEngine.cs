﻿using System;
using System.Collections.Generic;

namespace HDFqlDBMS.QueryEngine
{
    public interface IHDFqlEngine
    {
        List<string> CommandStack {get;set;}
        void ExecuteStack();
    }
}
