using System;
using System.Collections.Generic;

namespace HDFqlDBMS.QueryEngine
{
    public class HDFqlStandEngine :  IHDFqlEngine
    {
        protected List<string> _commandStack;
        public List<string> CommandStack
        {
            get
            {
                return _commandStack;
            }
            set
            {
                _commandStack = value;
            }
        }
        public HDFqlStandEngine()
        {
            _commandStack = new List<string>();
        }
        public void ExecuteStack()
        {
            for(int idx = 0; idx < _commandStack.Count;idx++)
            {
                AS.HDFql.HDFql.Execute(_commandStack[idx]);
            }
            _commandStack = new List<string>();
        }

    }
}
;