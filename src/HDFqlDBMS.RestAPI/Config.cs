using System;
using System.IO;

namespace HDFqlDBMS.RestAPI
{
    internal static class Config
    {
        internal static string GetContentRoot()
        {
            var contentRoot = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "wwwroot" + Path.DirectorySeparatorChar + "dataBaseFiles";
            return (contentRoot);
        }
    }
}