using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HDFqlDBMS.RestAPI.Controllers.Model.Interop;
using Microsoft.AspNetCore.Authentication;

namespace HDFqlDBMS.RestAPI.Controllers
{
    [Microsoft.AspNetCore.Authorization.Authorize]
    [Route("api/[controller]")]
    public class QueryController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "yes here is an api!";
        }

        [HttpPost]
        public void Post([FromBody] HDFqlCommand  value)
        {
            var engine = new HDFqlDBMS.QueryEngine.HDFqlStandEngine();

            engine.CommandStack.Add(value.Query);

            engine.ExecuteStack();
        }
    }
}