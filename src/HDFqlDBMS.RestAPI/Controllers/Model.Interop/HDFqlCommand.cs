using System;
using HDFqlDBMS.RestAPI.Controllers.Model.Interop;

namespace HDFqlDBMS.RestAPI.Controllers.Model.Interop
{
    public class HDFqlCommand : ICommand
    {
        protected string _query;
        public string Query 
        {
            get
            {
                return _query;
            }
            set
            {
                _query = value;
            }
        }
    }
}