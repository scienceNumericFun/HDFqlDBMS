using System;

namespace HDFqlDBMS.RestAPI.Controllers.Model.Interop
{
    public interface ICommand
    {
        string Query {get;set;}
    }
}